const puppeteer = require("puppeteer-extra");
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const randomUA = require("modern-random-ua");
const stealth = StealthPlugin();
stealth.enabledEvasions.delete('chrome.runtime')
stealth.enabledEvasions.delete('iframe.contentWindow')

const scraper = async () => {
	let browser, page;
	let pageUrl = 'https://xoso.me/xsmb-sxmb-xstd-xshn-kqxsmb-ket-qua-xo-so-mien-bac.html';

	try {
		const args = [
			'--no-sandbox',
			'--disable-setuid-sandbox',
			'--disable-infobars',
			'--window-position=0,0',
			'--ignore-certifcate-errors',
			'--ignore-certifcate-errors-spki-list',
			'--start-maximized'
		];

		const options = {
			args,
			headless: false,
			ignoreHTTPSErrors: true,
			ignoreDefaultArgs: ["--enable-automation"],
			defaultViewport: null,
		};

		browser = await puppeteer.launch(options);
		page = (await browser.pages())[0];

		await page.setUserAgent(randomUA.generate());
		await page.goto(pageUrl, { waitUntil: 'domcontentloaded' });

		const valueKq = await page.evaluate(async () => {
			const tableMB = document.querySelector('table.kqmb tbody')
			const childTable = Array.from(tableMB.querySelectorAll('tr'))
			return childTable.map((row, index)=> {
				if(index !== 0) {
					const values = Array.from(row.querySelectorAll('.v-giai span'))
					return {name: row.querySelector('.txt-giai').innerText, values: values.map((value)=> (value.innerText))}
				}
			})
		});
		console.log(valueKq)


	} catch (e) {
		console.log(e)
		browser.close()
	}
	finally {
		if (browser) await browser.close()
	}
}

scraper()
